﻿open System.Threading

let intToStr x = x.ToString


let intToSeqOfInts x = 
    x.ToString() |> Seq.map (int >> (+) -48)
    
printfn "%A"  <| intToSeqOfInts 123456

let areEqual (a, b) =
    a = b

let lte (a, b) =
    a <= b
    
let checkConsecutiveEqual s =
    s |> Seq.pairwise |> Seq.exists areEqual 

let checkMonotonic s =
    s |> Seq.pairwise |> Seq.forall lte

let checkValid x =
    checkConsecutiveEqual x && checkMonotonic x
    
let validCodes = seq {
    for candidate in [372304..847060] do
    if candidate |> intToSeqOfInts |> checkValid then
        yield candidate
}

validCodes |> Seq.length |> printfn "%d"
