﻿
let originalOpCodes = [|
           1; 0; 0; 3;
           1; 1; 2; 3;
           1; 3; 4; 3;
           1; 5; 0; 3;
           2; 10; 1; 19;
           1; 19; 9; 23;
           1; 23; 13; 27;
           1; 10; 27; 31;
           2; 31; 13; 35;
           1; 10; 35; 39;
           2; 9; 39; 43;
           2; 43; 9; 47;
           1; 6; 47; 51;
           1; 10; 51; 55;
           2; 55; 13; 59;
           1; 59; 10; 63;
           2; 63; 13; 67;
           2; 67; 9; 71;
           1; 6; 71; 75;
           2; 75; 9; 79;
           1; 79; 5; 83;
           2; 83; 13; 87;
           1; 9; 87; 91;
           1; 13; 91; 95;
           1; 2; 95; 99;
           1; 99; 6; 0;
           99; 2; 14; 0;
           0
           |]


let add (a, b) = a + b
let mul (a, b) = a * b

let runCode (arr: int [], idx: int) =
    let opcode = arr.[idx]
    let args = arr.[arr.[idx + 1]], arr.[arr.[idx + 2]]
    let writepos = arr.[idx + 3]
    // printfn "Opcode: %A" <| opcode
    // printfn "Args: %A" <| args
    let result = match opcode with
        | 1 -> add args
        | 2 -> mul args
        | _ -> -1
    // printfn "Result: %A" <| result
    // printfn "Writepos %A" <| writepos
    if result > 0 then
        arr.[writepos] <- result
        result
    else
        arr.[0]

let setState (arr:int[], verb:int, noun:int) =
    let new_arr:int[] = arr.[*]
    new_arr.[1] <- verb
    new_arr.[2] <- noun
    new_arr


let target = 19690720
let codeSeq = seq { for i in 0..(originalOpCodes.Length / 4 - 1) -> i * 4 }

for verb in [1..100] do
    for noun in [1..100] do
        printfn "trying: %i" (100 * verb + noun)
        let opCodes = setState(originalOpCodes, verb, noun)
        for idx in codeSeq do
            // printfn " %i: %A" idx <| opCodes.[idx..]
            // printfn "pos 0: %i" opCodes.[0]
            if runCode (opCodes, idx) = target then
                printfn "%i matches!" (100 * verb + noun)
                Operators.exit(1)
