
let fuelMass wt = wt |> Seq.unfold (fun x ->
    if (x > 0) then
        Some(x, x / 3 - 2)
    else
        None
        )


let calcFuelRequirement wt = fuelMass wt |> Seq.sum |> (+) -wt

let calcFuelMass s =
    s |> int |> calcFuelRequirement

System.IO.File.ReadLines("input1.txt") |> Seq.map calcFuelMass |> Seq.sum |> printfn "%A"
