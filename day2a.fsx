﻿
let opCodes = [|
           1; 0; 0; 3;
           1; 1; 2; 3;
           1; 3; 4; 3;
           1; 5; 0; 3;
           2; 10; 1; 19;
           1; 19; 9; 23;
           1; 23; 13; 27;
           1; 10; 27; 31;
           2; 31; 13; 35;
           1; 10; 35; 39;
           2; 9; 39; 43;
           2; 43; 9; 47;
           1; 6; 47; 51;
           1; 10; 51; 55;
           2; 55; 13; 59;
           1; 59; 10; 63;
           2; 63; 13; 67;
           2; 67; 9; 71;
           1; 6; 71; 75;
           2; 75; 9; 79;
           1; 79; 5; 83;
           2; 83; 13; 87;
           1; 9; 87; 91;
           1; 13; 91; 95;
           1; 2; 95; 99;
           1; 99; 6; 0;
           99; 2; 14; 0;
           0
           |]

Array.length <| opCodes |> printfn "%A"

let add (a, b) = a + b
let mul (a, b) = a * b

let runCode (arr: int [], idx: int) =
    let opcode = arr.[idx]
    let args = arr.[arr.[idx + 1]], arr.[arr.[idx + 2]]
    let writepos = arr.[idx + 3]
    printfn "Opcode: %A" <| opcode
    printfn "Args: %A" <| args
    let result = match opcode with
        | 1 -> add args
        | 2 -> mul args
        | _ -> -1
    printfn "Result: %A" <| result
    printfn "Writepos %A" <| writepos
    if result > 0 then
        arr.[writepos] <- result

opCodes.[1] <- 12
opCodes.[2] <- 2
let codeSeq = seq { for i in 0..(opCodes.Length / 4 - 1) -> i * 4 }
for idx in codeSeq do
    printfn " %i: %A" idx <| opCodes.[idx..]
    printfn "pos 0: %i" opCodes.[0]
    runCode (opCodes, idx) 
