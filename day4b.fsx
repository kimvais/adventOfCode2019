﻿let intToSeqOfInts x =
    x.ToString() |> Seq.map (int >> (+) -48)

let areEqual (a, b) =
    a = b

let lte (a, b) =
    a <= b

let checkConsecutiveEqual s =
    s |> Seq.pairwise |> Seq.exists areEqual

let checkMonotonic s =
    s |> Seq.pairwise |> Seq.forall lte

let checkValid x =
    checkConsecutiveEqual x && checkMonotonic x

let validCodesForPart1 = seq {
    for candidate in [ 372304..847060 ] do
    if candidate |> intToSeqOfInts |> checkValid then
        yield candidate
 }

let checkThatGroupOfTwoExists s =
    s |> Seq.groupBy (fun x -> x) |> Seq.exists (fun (_, b) -> Seq.length b = 2)

validCodesForPart1|> Seq.map intToSeqOfInts|> Seq.filter checkThatGroupOfTwoExists|> Seq.length|> printfn "%A"

